package net.opentaskforce.edu.rsa;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * A simple (and poor) RSA implementation.<p>
 * <b>Not intended to be used in production systems.</b>
 * 
 * @author Harald Lang
 */
public class RSA {
	
	private RSA(){};

	/**
	 * Generates a public-/private-key pair.
	 * @param bitLength the number of bits
	 * @return a key pair.
	 */
	public static KeyPair generateKeys(int bitLength) {
		final SecureRandom random = new SecureRandom();
		
		BigInteger p = BigInteger.probablePrime(bitLength / 2, random);
		BigInteger q;
		do {
			q = BigInteger.probablePrime(bitLength / 2, random);
		} while (p.equals(q));  // make sure that p and q are different
		
		BigInteger n = p.multiply(q);
		
		// Euler's phi function 
		BigInteger phiOfN = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
		
		
		BigInteger e  = p.max(q).nextProbablePrime();
		assert(e.compareTo(phiOfN) < 0);
		BigInteger d = e.modInverse(phiOfN);

		KeyPair k = new KeyPair();
		// private key: (n,d)
		k.privateKey.n = n;
		k.privateKey.d = d;
		// public key: (n,e)  
		k.publicKey.n = n;
		k.publicKey.e = e;
		return k;
	}

	/**
	 * Encrypts a message with RSA.
	 * 
	 * @param m the message to encrypt
	 * @param k the receivers public-key 
	 * @return the encrypted message (ciphertext)
	 */
	public static BigInteger encrypt(BigInteger m, PublicKey k) {
		return m.modPow(k.e, k.n);
	}
	
	/**
	 * Convenience method to encrypt strings.
	 * 
	 * @see #encrypt(BigInteger, PublicKey)
	 * 
	 * @param m the message to encrypt
	 * @param k the receivers public-key 
	 * @return the encrypted message (ciphertext)
	 */
	public static BigInteger encryptString(String m, PublicKey k) {
		BigInteger mi = new BigInteger(m.getBytes());
		return mi.modPow(k.e, k.n);
	}
	
	/**
	 * Decrypts a RSA encrypted ciphertext. 
	 * @param c the ciphertext to decrypt
	 * @param k the <em>receivers</em> private-key
	 * @return the decrypted plain-text.
	 */
	public static BigInteger decrypt(BigInteger c, PrivateKey k) {
		return c.modPow(k.d, k.n);
	}

	/**
	 * Convenience method to dencrypt strings.
	 * 
	 * @see #decrypt(BigInteger, PrivateKey)
	 * 
	 * @param m the message to encrypt
	 * @param k the <em>receivers</em> public-key 
	 * @return the encrypted message (ciphertext)
	 */
	public static String decryptString(BigInteger c, PrivateKey k) {
		final BigInteger mi = c.modPow(k.d, k.n);
		return new String(mi.toByteArray());
	}

	/**
	 * Signs a message with RSA.
	 * 
	 * @param m the message to sign
	 * @param k the <em>signers</em> private-key 
	 * @return the signature of the message
	 */
	public static BigInteger sign(BigInteger m, PrivateKey k) {
		return m.modPow(k.d, k.n);
	}
	
	/**
	 * Convenience method to sign strings.
	 * 
	 * @see #sign(BigInteger, PrivateKey)
	 * 
	 * @param m the message to sign
	 * @param k the <em>signers</em> private-key 
	 * @return the signature of the message
	 */
	public static BigInteger signString(String m, PrivateKey k) {
		BigInteger mi = new BigInteger(m.getBytes());
		return mi.modPow(k.d, k.n);
	}
	
	/**
	 * Verifies the signature of a message. 
	 * 
	 * @param m the message in plain-text
	 * @param s the signature
	 * @param publicKey the <em>signers</em> public-key
	 * @return
	 */
	public static boolean verify(BigInteger m, BigInteger s, final PublicKey publicKey) {
		return s.modPow(publicKey.e, publicKey.n).equals(m);
	}

	/**
	 * Verifies the signature of a string message. 
	 * 
	 * @param m the message in plain-text
	 * @param s the signature
	 * @param publicKey the <em>signers</em> public-key
	 * @return
	 */
	public static boolean verifyString(String m, BigInteger s, final PublicKey publicKey) {
		BigInteger mi = new BigInteger(m.getBytes());
		return s.modPow(publicKey.e, publicKey.n).equals(mi);
	}

	/**
	 * A data structure for a private-/public-key pair.
	 */
	public static class KeyPair {
		public PrivateKey privateKey = new PrivateKey();
		public PublicKey publicKey = new PublicKey();
	}

	/**
	 * A data structure for a private-key.
	 */
	public static class PrivateKey {
		/** Modulus */
		public BigInteger n;
		/** Decryption exponent */
		public BigInteger d;
	}

	/**
	 * A data structure for a public-key.
	 */
	public static class PublicKey {
		/** Modulus */
		public BigInteger n;
		/** Encryption exponent */
		public BigInteger e;
	}

}
