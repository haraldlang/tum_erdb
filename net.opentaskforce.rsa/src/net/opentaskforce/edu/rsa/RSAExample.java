package net.opentaskforce.edu.rsa;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import net.opentaskforce.edu.rsa.RSA.KeyPair;

public class RSAExample {

	public static void main(String[] args) throws NoSuchAlgorithmException {
		// Preparation
		// Generate key-pairs for Alice and Bob.
		KeyPair alice = RSA.generateKeys(512);
		KeyPair bob = RSA.generateKeys(512);

		// ---------------------------------------------------------------------
		// Example 1: Alice sends a encrypted message to Bob.
		// ---------------------------------------------------------------------
		{
			String message = "Top secret!";
			System.out.println("Alice' message: " + message);
			// Alice encrypts the message with Bob's public-key.
			BigInteger ciphertext = RSA.encryptString(message, bob.publicKey);
			System.out.println("Encrypted message: " + ciphertext);
			// The ciphertext is sent to Bob.
			
			// Bob uses his private-key to decrypt the message.
			String decryptedMessage = RSA.decryptString(ciphertext, bob.privateKey);
			System.out.println("Decrypted message: " + decryptedMessage);
			
			System.out.println("------");
		}

		// ---------------------------------------------------------------------
		// Example 2: Alice sends a signed message to Bob.
		// ---------------------------------------------------------------------
		{
			String message = "Hello Bob!";
			System.out.println("Alice' message: " + message);
			// Alice signs the message with her own private-key.
			BigInteger signature = RSA.signString(message, alice.privateKey);
			System.out.println("Signature: " + signature);
			// The message and the signature are sent to Bob.
			
			// Bob uses Alice' public-key to verify the signature.
			boolean verified = RSA.verifyString(message, signature, alice.publicKey);
			System.out.println("Signature valid: " + verified);
			System.out.println("------");
		}

		// ---------------------------------------------------------------------
		// Example 3: Alice signs a large message and sends it to Bob.
		// ---------------------------------------------------------------------
		{
			
			String message = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam";
			System.out.println("Alice' message: " + message);
			// In this case the message is to long to sign it with a 512 bit key.
			// Therefore Alice computes a fingerprint using a cryptographic hash function...
			String fingerprint = shaHash(message);
			System.out.println("SHA Fingerprint: " + fingerprint);
			// ... and signs it with her own private-key.
			BigInteger signature = RSA.signString(fingerprint, alice.privateKey);
			System.out.println("SHA/Signature: " + signature);
			// The message and the signature are sent to Bob.
			
			
			// Bob also computes the fingerprint...
			String bobFingerprint = shaHash(message);
			// ... and uses Alice' public-key to verify the signature.
			boolean verified = RSA.verifyString(bobFingerprint, signature, alice.publicKey);
			System.out.println("SHA/Signature valid: " + verified);
			System.out.println("------");
		}

	}

	private static String shaHash(String m) throws NoSuchAlgorithmException {
		MessageDigest sha = MessageDigest.getInstance( "SHA" );
		byte[] digest = sha.digest(m.getBytes());
		return toHex(digest);
	}
	
	private static String toHex(byte[] bytes) {
	    BigInteger bi = new BigInteger(1, bytes);
	    return String.format("%0" + (bytes.length << 1) + "X", bi);
	}
	
}
