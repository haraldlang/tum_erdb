package net.opentaskforce.edu.rsa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigInteger;

import net.opentaskforce.edu.rsa.RSA;

import org.junit.After;
import org.junit.Test;

public class RSATest {
	
	@After
	public void after() {
		System.out.println("-----------------------------------------");
	}
	
	@Test
	public void encryptDecrypt() {
		RSA.KeyPair k = RSA.generateKeys(128);
		BigInteger m = BigInteger.valueOf(1337);
		System.out.println("Plaintext: " + m);
		BigInteger c = RSA.encrypt(m, k.publicKey);
		System.out.println("Ciphertext: " + c);
		System.out.println("Decrypted: " + RSA.decrypt(c, k.privateKey));
		assertEquals(m, RSA.decrypt(c, k.privateKey));
	}

	@Test
	public void encryptDecrypt_shouldFailWhenCiphertextHasBeenModified() {
		RSA.KeyPair k = RSA.generateKeys(128);
		BigInteger m = BigInteger.valueOf(1337);
		System.out.println("Plaintext: " + m);
		BigInteger c = RSA.encrypt(m, k.publicKey);
		c = c.add(BigInteger.ONE);
		System.out.println("Ciphertext: " + c);
		System.out.println("Decrypted: " + RSA.decrypt(c, k.privateKey));
		assertFalse(m.equals(RSA.decrypt(c, k.privateKey)));
	}

	@Test
	public void encryptDecryptString() {
		RSA.KeyPair k = RSA.generateKeys(128);
		String m = "TUM";
		System.out.println("Plaintext: " + m);
		BigInteger c = RSA.encryptString(m, k.publicKey);
		System.out.println("Ciphertext: " + c);
		System.out.println("Decrypted: " + RSA.decryptString(c, k.privateKey));
		assertEquals(m, RSA.decryptString(c, k.privateKey));
	}

	@Test
	public void encryptDecryptStringWithLargeKeys() {
		RSA.KeyPair k = RSA.generateKeys(2048);
		String m = "Technische Universitaet Muenchen";
		System.out.println("Plaintext: " + m);
		BigInteger c = RSA.encryptString(m, k.publicKey);
		System.out.println("Ciphertext: " + c);
		System.out.println("Decrypted: " + RSA.decryptString(c, k.privateKey));
		assertEquals(m, RSA.decryptString(c, k.privateKey));
	}

	@Test
	public void signVerify() {
		RSA.KeyPair k = RSA.generateKeys(128);
		BigInteger m = BigInteger.valueOf(1337);
		BigInteger s = RSA.sign(m, k.privateKey);
		
		System.out.println("Plaintext: " + m);
		System.out.println("Signature: " + s);
		
		System.out.println("Verification: " + RSA.verify(m, s, k.publicKey));
		assertTrue(RSA.verify(m, s, k.publicKey));
	}

	@Test
	public void signVerifyString() {
		RSA.KeyPair k = RSA.generateKeys(128);
		String m = "TUM";
		BigInteger s = RSA.signString(m, k.privateKey);
		
		System.out.println("Plaintext: " + m);
		System.out.println("Signature: " + s);
		
		System.out.println("Verification: " + RSA.verifyString(m, s, k.publicKey));
		assertTrue(RSA.verifyString(m, s, k.publicKey));
	}

}
